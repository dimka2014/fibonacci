package com.belyaev.fibonacci;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Worker implements Runnable {
    protected int[] fibonacci;
    protected List<Integer> list;
    protected int divisor;
    protected int remainder;

    public Worker(int[] fibonacci, List<Integer> list, int divisor, int remainder) {
        this.fibonacci = fibonacci;
        this.list = list;
        this.divisor = divisor;
        this.remainder = remainder;
    }

    @Override
    public void run() {
        for (Integer entry: list) {
            if (entry % divisor == remainder) {
                List<Integer> combination = new LinkedList<>();
                int found = Arrays.binarySearch(fibonacci, entry);
                int tmp = entry;
                while (found < 0) {
                    combination.add(fibonacci[-found - 2]);
                    tmp -= fibonacci[-found - 2];
                    found = Arrays.binarySearch(fibonacci, tmp);
                }
                combination.add(fibonacci[found]);
//                System.out.println(entry + "->" + combination);
            }
        }
    }
}
